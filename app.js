/*   

**  Module dependencies **
**Everlive and Express

*/

var Everlive = require('everlive-sdk');
var express = require('express');
var http = require('http');
var app = express();
var PORT = 8000;

//APP_ID for the old and new database from telerik backend Service Platform
var APP_ID_OLD = 'cl8mwlp8x7hrotvp';
var APP_ID_NEW = 'be4w923x4mpxklzh';
var SCHEME = 'htpps';

//Add data from everlive old backend table 'Deals' to new backend table 'Deal'
app.get('/data/InsertDeal',function(request,response){
   
  var _el = new Everlive({
      appId: APP_ID_OLD,
      scheme: SCHEME
  });
  
  var query = new Everlive.Query();
  query.where()
       .eq('Id','6fea6200-fb38-11e5-801c-573bec770c9e')
       .done()
        .select('City','Description','Location','Phone',
       'Street','Summary','Terms','Title',
       'Type','Website');       
  _el.data('Deals').get(query).then(function(actualdata){
  console.log('Retrieving the query data ');
  // response.status(200).json(actualdata);
   console.log(actualdata);
   console.log('Successfully done first part!!! ');
   
   if(actualdata){
       var _el = new Everlive({
           appId: APP_ID_NEW,
           scheme:SCHEME
       })
       
       _el.data('Deal').create(actualdata.result).then(function(actualdata){
        console.log('Adding single record to the new database ');
         response.status(200).json(actualdata);
        console.log('Successfully done the second part too!!! ');
        });
       
   }
   
  }).catch(err => {
      response.send({success: false, msg: 'Error creating the data', err: err });
      
    })
});


//Update data from everlive old backend table 'Deals' to new backend table 'Deal'
//Update the fields which have different naming when compared to the new backend structure
app.get('/data/UpdateDeal',function(request,response){
    
     var _el = new Everlive({
      appId: APP_ID_OLD,
      scheme: SCHEME
  });
  
  var query = new Everlive.Query();
  query.where()
       .eq('Id','6fea6200-fb38-11e5-801c-573bec770c9e')
       .done()
       .select('Availability','Postcode','offer','OfferCode');
  _el.data('Deals').get(query).then(function(actualdata){
  console.log('Retrieving the query data ');
  // response.status(200).json(actualdata);
   console.log(actualdata);
   console.log('Successfully done first part!!! ');
   
 if(actualdata){
   var _el = new Everlive({
           appId: APP_ID_NEW,
           scheme:SCHEME
       });
       
          _el.data('Deal').updateSingle({Id : '6fea6200-fb38-11e5-801c-573bec770c9e',
            'Available': actualdata.result[0].Availability,
           'PostCode' : actualdata.result[0].Postcode, 
           'Offer' : actualdata.result[0].offer, 
           'OfferCode' : actualdata.result[0].offerCode})
          .then(function(actualdata){
        console.log('Adding single record to the new database ');
         response.status(200).json(actualdata);
        console.log('Successfully done the second part too!!! '); 
      });
 }     
  }).catch(err => {
      response.send({success: false, msg: 'Error updating the data', err: err });
      
    })
});

//Get Id to old backend table 'Deals'
app.get('/data/getDealId',function(request,response){
   
  var _el = new Everlive({
      appId: APP_ID_OLD,
      scheme: SCHEME
  });
  
  var query = new Everlive.Query();
  //var query = new Everlive.AggregateQuery();
  query.where().done().select('Id','Merchant');
  //query.groupBy(['ShopId']);
  //query.count('Type');
  _el.data('Deals').get(query).then(function(actualdata){
  console.log('Retrieving the query data ');
  response.status(200).json(actualdata);
  // console.log(actualdata);
   console.log('Successfully done !!! ');
     }).catch(err => {
      response.send({success: false, msg: 'Error getting the data', err: err });
      
    })
});

//Get Files from old backend table 'Deals'
app.get('/data/getFiles',function(request,response){
   
  var _el = new Everlive({
      appId: APP_ID_OLD,
      scheme: SCHEME
  });
  
 
  _el.Files.get().then(function(actualdata){
  console.log('Retrieving the query data ');
  response.status(200).json(actualdata);
  // console.log(actualdata);
   console.log('Successfully done !!! ');
     }).catch(err => {
      response.send({success: false, msg: 'Error getting the data', err: err });
      
    })
});


//Get Id to new backend table 'Deal'
app.get('/data/getDealIdNew',function(request,response){
   
  var _el = new Everlive({
      appId: APP_ID_NEW,
      scheme: SCHEME
  });
  
  //var query = new Everlive.Query();
 var query = new Everlive.AggregateQuery();
  //query.select('Id','Title');
  query.groupBy(['Vendor']);
  //query.count('Vendor');
  
  _el.data('Deal').aggregate(query).then(function(actualdata){
  console.log('Retrieving the query data ');
  response.status(200).json(actualdata);
  // console.log(actualdata);
   console.log('Successfully done !!! ');
     }).catch(err => {
      response.send({success: false, msg: 'Error getting the data', err: err });
      
    })
});


/***********************Image table*************************** */

/***********************Files Data*************************** */
//Add data from everlive old backend 'Files' to new backend table 'Files'
app.get('/data/InsertFilesOldToNew',function(request,response){
   
  var _el = new Everlive({
      appId: APP_ID_OLD,
      scheme: SCHEME
  });
  
 // var query = new Everlive.Query();
  //query.select('Id');
 _el.Files.getById('abf450b0-1ce6-11e6-a940-71bfac8604d7').then(function(data){
  console.log('Retrieving the file data ');
  //response.status(200).json(data);
  console.log(data);
   console.log('Successfully done first part!!! ');
 
   if(data){
   var _el = new Everlive({
           appId: APP_ID_NEW,
           scheme:SCHEME
       });
      console.log(data.result[0].Id);
       console.log(data.result[0]);
     //  var file = {'Id':'abf450b0-1ce6-11e6-a940-71bfac8604d7','Filename': 'data.result[0].Filename'};
          _el.Files.create(data.result)
          .then(function(data){
             // console.log(data.result[0].Filename);
        console.log('Adding file record to the new Files section ');
         response.status(200).json(data);
        console.log('Successfully done the second part too!!! '); 
      });
 }   
     }).catch(err => {
      response.send({success: false, msg: 'Error getting the data', err: err });
      
    })
});


//Download Files from the telerik URL
app.get('/data/DownloadFiles',function(request,response){
    var _el = new Everlive({
      appId: APP_ID_OLD,
      scheme: SCHEME
  });

    var fileId ='0f4fe050-1695-11e6-b51a-f57a6b26d48d'; // the file identifier is retrived from the RESTful services
    _el.files.getDownloadUrlById(fileId)
    .then(function(downloadUrl){
        console.log("Downloading file");
       // document.getElementById('anchor1').href = downloadUrl;
       console.log(downloadUrl);
       response.status(200).json(downloadUrl); 
       
    }).catch(err =>{
        response.send({success: false, msg: 'Error getting the url', err: err });
    })
    
    });
    
  //Insert New pictures into  Image table.
app.get('/data/UploadToImage',function(request,response){
  
  var _el = new Everlive({
      appId: APP_ID_NEW,
      scheme: SCHEME
  });
  
  var query = new Everlive.Query();
  query.where()
       .eq('Id','a22cd3c0-33db-11e6-b052-f78db3d24b6b')
       .done()
        .select();
  _el.Files.get(query).then(function(actualdata){
  console.log('Retrieving the query data ');
  //response.status(200).json(actualdata);
  console.log('Done!!');
  console.log(actualdata);
   _el.data('Image')
  .create({'Name': 'Witchway',
  'File' : actualdata.result[0].Id})
    .then(function(actualdata){
  console.log('Retrieving the query data ');
    response.status(200).json(actualdata);
  console.log('Successfully done!!!')
});
    
  }).catch(err => {
      response.send({success: false, msg: 'Error creating the data', err: err });
      
    })
});  
    
    
    


/***********************Vendor table*************************** */
//Add data from everlive old backend table 'NearMe' to new backend table 'Vendor/VendorDeals'
app.get('/data/getVendor',function(request,response){
   
  var _el = new Everlive({
      appId: APP_ID_OLD,
      scheme: SCHEME
  });
  
  var query = new Everlive.Query();
  query.where()
       .eq('Id','75589e30-1b7e-11e6-8bed-4de46612f74d')
       .done()
       .select('Id','Brief','Title');
  _el.data('NearMe').get(query).then(function(actualdata){
  console.log('Retrieving the query data ');
  response.status(200).json(actualdata);
  // console.log(actualdata);
   console.log('Successfully done first part!!! ');
     }).catch(err => {
      response.send({success: false, msg: 'Error creating the data', err: err });
      
    })
});



//Add data from everlive old backend table 'NearMe' to new backend table 'Vendor'
app.get('/data/InsertVendor',function(request,response){
   
  var _el = new Everlive({
      appId: APP_ID_OLD,
      scheme: SCHEME
  });
  
  var query = new Everlive.Query();
  query.where()
       .eq('Id','42b06830-32dc-11e6-8129-ddfc786c6074')
       .done()
       .select('Id','Brief','Location');
  _el.data('NearMe').get(query).then(function(actualdata){
  console.log('Retrieving the query data ');
  //response.status(200).json(actualdata);
   console.log(actualdata);
   console.log('Successfully done first part!!! ');
   
   if(actualdata){
       var _el = new Everlive({
           appId: APP_ID_NEW,
           scheme:SCHEME
       })
      
       console.log(actualdata);
       _el.data('Vendor')
          .create(actualdata.result)
          .then(function(actualdata){
        console.log('Adding multiple record to the new database ');
         response.status(200).json(actualdata);
         console.log('Successfully done the second part too!!! ');
        });     
     }    
  }).catch(err => {
      response.send({success: false, msg: 'Error creating the data', err: err });
      
    })
});


//Update name field from everlive old backend table 'NearMe' to new backend table 'Vendor' 
app.get('/data/UpdateVendor',function(request,response){
  
   var _el = new Everlive({
      appId: APP_ID_OLD,
      scheme: SCHEME
  });
  
  var query = new Everlive.Query();
  query.where()
       .eq('Id','75589e30-1b7e-11e6-8bed-4de46612f74d')
       .done()
       .select('Title');
  _el.data('NearMe').get(query).then(function(actualdata){
  console.log('Retrieving the query data ');
  //response.status(200).json(actualdata);
   console.log(actualdata);
   console.log('Successfully done first part!!! ');
  
 if(actualdata){
   var _el = new Everlive({
           appId: APP_ID_NEW,
           scheme:SCHEME
       });
       
          _el.data('Vendor').updateSingle({Id : '75589e30-1b7e-11e6-8bed-4de46612f74d',
            'Name': actualdata.result[0].Title })
          .then(function(actualdata){
        console.log('Adding single record to the new database ');
         response.status(200).json(actualdata);
        console.log('Successfully done the second part too!!! '); 
      });
 }   
  }).catch(err => {
      response.send({success: false, msg: 'Error updating the data', err: err });
      
    })
});

//Add vendor ID from everlive old backend table 'Deals' to new backend table 'Deal'
app.get('/data/UpdateVendorID',function(request,response){
  var _el = new Everlive({
      appId: APP_ID_OLD,
      scheme: SCHEME
  });
  
  var query = new Everlive.Query();
  query.where()
       .eq('Id','7eac84b0-1ce6-11e6-b247-df34e0fe9a8b')
       .done()
       .select('ShopId');
  _el.data('Deals').get(query).then(function(actualdata){
  console.log('Retrieving the query data ');
  // response.status(200).json(actualdata);
   console.log(actualdata);
   console.log('Successfully done first part!!! ');
   
 if(actualdata){
   var _el = new Everlive({
           appId: APP_ID_NEW,
           scheme:SCHEME
       });
       
          _el.data('Deal').updateSingle({Id : '7eac84b0-1ce6-11e6-b247-df34e0fe9a8b',
            'Vendor': actualdata.result[0].ShopId})
          .then(function(actualdata){
        console.log('Adding single record to the new database ');
         response.status(200).json(actualdata);
        console.log('Successfully done the second part too!!! '); 
      });
 }     
  }).catch(err => {
      response.send({success: false, msg: 'Error updating the data', err: err });
      
    }) 
  
});


//Add data from everlive new backend Files to new backend table 'Image'
app.get('/data/InsertImage',function(request,response){
   
  var _el = new Everlive({
      appId: APP_ID_NEW,
      scheme: SCHEME
  });
  
  var query = new Everlive.Query();
  //query.where().eq('Id','069faf30-c0ff-11e5-82c0-f5837dca8990').done().select('Id','Name');
  _el.Files.get().then(function(actualdata){
  console.log('Retrieving the Files data ');
  //response.status(200).json(actualdata);
   console.log(actualdata);
   console.log('Successfully done first part!!! ');
  actualdata.result.forEach(function(Item,key){
  _el.data('Image').create({'Name' :Item.Filename ,'File' : Item.Id})
   .then(function(data){
      // console.log(actualdata.result);
       
       response.status(200).json(data);
       console.log("Updated!");
   });
   });
     }).catch(err => {
      response.send({success: false, msg: 'Error creating the data', err: err });
      
    })
});


//Update Image field in new backend table 'Deal'
app.get('/data/UpdateImage',function(request,response){
   
  var _el = new Everlive({
      appId: APP_ID_NEW,
      scheme: SCHEME
  });
  
  var query = new Everlive.Query();
  //query.where().eq('Id','90969d20-2d42-11e6-ad24-dd368cb7f771').done().select();
  //_el.data('Image').get(query).then(function(actualdata){
      _el.Files.getById('90969d20-2d42-11e6-ad24-dd368cb7f771').then(function(actualdata){
  console.log('Retrieving the query data ');
  //response.status(200).json(actualdata);
   console.log(actualdata);
   console.log('Successfully done first part!!! ');
  if(actualdata){
     
      console.log(actualdata.result[0].Id);
    _el.data('Deal').update({'Id' : '069faf30-c0ff-11e5-82c0-f5837dca8990',
    'Image' : actualdata.result[0].Id})
   .then(function(actualdata){
       console.log(actualdata.result[0].Id);
       console.log("Updated!");
       response.status(200).json(actualdata);
  });
  }
     }).catch(err => {
      response.send({success: false, msg: 'Error updating the data', err: err });
      
    })
});




//Get Events from old backend table 'Deals'
app.get('/data/getEvent',function(request,response){
   
  var _el = new Everlive({
      appId: APP_ID_OLD,
      scheme: SCHEME
  });
  
  var query = new Everlive.Query();
  //var query = new Everlive.AggregateQuery();
  query.where().eq('Type','Event').done().select('Id','ShopId','Type','Merchant','Availability');
  //query.groupBy(['Type','ShopId']);
  //query.count('Type');
  _el.data('Deals').get(query).then(function(actualdata){
  console.log('Retrieving the query data ');
  response.status(200).json(actualdata);
  // console.log(actualdata);
   console.log('Successfully done !!! ');
     }).catch(err => {
      response.send({success: false, msg: 'Error getting the data', err: err });
      
    })
});


//Add records to events table from new backend table 'Deal'
app.get('/data/AddEvent',function(request,response){
   
  var _el = new Everlive({
      appId: APP_ID_NEW,
      scheme: SCHEME
  });
  
  var query = new Everlive.Query();
  query.where().and().eq('Id','30f69ee0-af0e-11e5-8368-558339cae6f9').done();
 query.select('Id','Description','Street', 'PostCode','Terms','Location','Vendor','Title',
 'Summary','City','Country','Phone','Website');
  
  _el.data('Deal').get(query).then(function(actualdata){
  console.log('Retrieving the query data ');
  //response.status(200).json(actualdata);
   console.log(actualdata.result);
     console.log('Successfully done !!! ');
     if(actualdata){
         _el.data('Event').create(actualdata.result).then(function(actualdata){
            response.status(200).json(actualdata); 
            console.log('Finally Done!!!')
         });
     }
     }).catch(err => {
      response.send({success: false, msg: 'Error getting the data', err: err });
      
    })
});

//Update Country field to events table 
app.get('/data/UpdateCountry',function(request,response){
   
  var _el = new Everlive({
      appId: APP_ID_NEW,
      scheme: SCHEME
  });
  
  var query = new Everlive.Query();
  
 // query.where().and().eq('Id','a3499690-c35a-11e5-97f2-6351430c1970').eq('Type','Event').done();
// query.select('Id','Description','Street', 'PostCode','Terms','Location','Vendor','Title',
 //'Summary','City','Country','Phone','Website');
  query.where().ne('Country','United Kingdom');
  _el.data('Event').update({'Country':'United Kingdom'},query).then(function(actualdata){
  console.log('Retrieving the query data ');
  response.status(200).json(actualdata);
  // console.log(actualdata.result);
     console.log('Successfully done !!! ');
     
     }).catch(err => {
      response.send({success: false, msg: 'Error getting the data', err: err });
      
    })
});

//Add records to events table from old backend table 'Deals' which have empty shopid
app.get('/data/AddEventEmptyShop',function(request,response){
   
  var _el = new Everlive({
      appId: APP_ID_OLD,
      scheme: SCHEME
  });
  
  var query = new Everlive.Query();
  query.where().and().eq('Id','8d840d00-c5a7-11e5-8bdb-c95f8623bd95').eq('Type','Event').done();
 query.select('Id','Description','Street','Terms','Location','Title',
 'Summary','City','Country','Phone','Website');
  
  _el.data('Deals').get(query).then(function(actualdata){
  console.log('Retrieving the query data ');
  //response.status(200).json(actualdata);
   console.log(actualdata.result);
     console.log('Successfully done !!! ');
     if(actualdata){
         var _el = new Everlive({
      appId: APP_ID_NEW,
      scheme: SCHEME
  });
         _el.data('Event').create(actualdata.result).then(function(actualdata){
            response.status(200).json(actualdata); 
            console.log('Finally Done!!!')
         });
     }
     }).catch(err => {
      response.send({success: false, msg: 'Error getting the data', err: err });
      
    })
});

//Add travel dealrecords to events table from new backend table 'TravelDeal'
app.get('/data/AddTravelDeal',function(request,response){
   
  var _el = new Everlive({
      appId: APP_ID_NEW,
      scheme: SCHEME
  });
  
  var query = new Everlive.Query();
  query.where().eq('Id','7b68c6e0-b555-11e5-b970-4bc42e52cb39').done();
 query.select('Id','Title','Summary','Description','Offer','Terms','Website');
  
  _el.data('Deal').get(query).then(function(actualdata){
  console.log('Retrieving the query data ');
  //response.status(200).json(actualdata);
   console.log(actualdata.result);
     console.log('Successfully done !!! ');
     if(actualdata){
         _el.data('TravelDeal').create(actualdata.result).then(function(actualdata){
            response.status(200).json(actualdata); 
            console.log('Finally Done!!!')
         });
     }
     }).catch(err => {
      response.send({success: false, msg: 'Error getting the data', err: err });
      
    })
});

//Add Vendor dealrecords from deal table to table 'VendorDeals'
app.get('/data/AddVendorDeal',function(request,response){
   
  var _el = new Everlive({
      appId: APP_ID_NEW,
      scheme: SCHEME
  });
  
  var query = new Everlive.Query();
  query.where().eq('Vendor','42b06830-32dc-11e6-8129-ddfc786c6074').done();
 query.select('Id','Title','Summary','Description','City','Street','Offer','OfferCode','Phone','PostCode','Vendor','Terms','Website');
  
  _el.data('Deal').get(query).then(function(actualdata){
  console.log('Retrieving the query data ');
  //response.status(200).json(actualdata);
   console.log(actualdata.result);
     console.log('Successfully done !!! ');
     if(actualdata){
         _el.data('VendorDeals').create(actualdata.result).then(function(actualdata){
            response.status(200).json(actualdata); 
            console.log('Finally Done!!!')
         });
     }
     }).catch(err => {
      response.send({success: false, msg: 'Error getting the data', err: err });
      
    })
});

//Add Vendor deal records from 'Deals' Table to new backend table 'VendorDeals'
app.get('/data/AddVendorDealFromOld',function(request,response){
   
  var _el = new Everlive({
      appId: APP_ID_OLD,
      scheme: SCHEME
  });
  
  var query = new Everlive.Query();
  //query.where().eq('Vendor','d84e2740-c5d3-11e5-9c39-ad4b2dbbb011').done();
 //query.select('Id','Title','Summary','Description','City','Street','Offer','OfferCode','Phone','PostCode','Vendor','Terms','Website');
  query.where()
       .eq('Id','3fda04c0-32de-11e6-8f91-311024870c49')
       .done()
        .select('Availability','City','Description','Phone',
       'Street','Summary','Terms','Title',
       'Website');
  _el.data('Deals').get(query).then(function(actualdata){
  console.log('Retrieving the query data ');
  //response.status(200).json(actualdata);
   console.log(actualdata.result);
     console.log('Successfully done !!! ');
     if(actualdata){
          var _el = new Everlive({
             appId: APP_ID_NEW,
             scheme: SCHEME
       });
         _el.data('VendorDeals').create(actualdata.result).then(function(actualdata){
            response.status(200).json(actualdata); 
            console.log('Finally Done!!!')
         });
     }
     }).catch(err => {
      response.send({success: false, msg: 'Error getting the data', err: err });
      
    })
});

    
//Update data from everlive old backend table 'Deals' to new backend table 'VendorDeals'
//Update the fields which have different naming when compared to the new backend structure
app.get('/data/UpdateVendorDealFromOld',function(request,response){
    
     var _el = new Everlive({
      appId: APP_ID_OLD,
      scheme: SCHEME
  });
  
  var query = new Everlive.Query();
  query.where()
       .eq('Id','3fda04c0-32de-11e6-8f91-311024870c49')
       .done()
       .select('Postcode','offer','OfferCode','ShopId');
  _el.data('Deals').get(query).then(function(actualdata){
  console.log('Retrieving the query data ');
  // response.status(200).json(actualdata);
   console.log(actualdata);
   console.log('Successfully done first part!!! ');
   
 if(actualdata){
   var _el = new Everlive({
           appId: APP_ID_NEW,
           scheme:SCHEME
       });
       
          _el.data('VendorDeals').updateSingle({Id : '3fda04c0-32de-11e6-8f91-311024870c49',
            'Vendor': actualdata.result[0].ShopId,
           'PostCode' : actualdata.result[0].Postcode, 
           'Offer' : actualdata.result[0].offer, 
           'OfferCode' : actualdata.result[0].offerCode})
          .then(function(actualdata){
        console.log('Adding single record to the new database ');
         response.status(200).json(actualdata);
        console.log('Successfully done the second part too!!! '); 
      });
 }     
  }).catch(err => {
      response.send({success: false, msg: 'Error updating the data', err: err });
      
    })
});    

//Update OfferCode field to the new table deals wherever applicabl eg: BUZ01OAK
app.get('/data/UpdateOfferCode',function(request,response){
   
  var _el = new Everlive({
      appId: APP_ID_OLD,
      scheme: SCHEME
  });
  
  var query = new Everlive.Query();
  
  query.where()
       .eq('Id','88b57760-a77e-11e5-9d84-794973810f39')
       .done()
        .select('offerCode');
  _el.data('Deals').get(query).then(function(actualdata){
  console.log('Retrieving the query data ');
  //response.status(200).json(actualdata);
   console.log(actualdata.result);
     console.log('Successfully done !!! ');
     if(actualdata){
          var _el = new Everlive({
             appId: APP_ID_NEW,
             scheme: SCHEME
       });
        _el.data('VendorDeals').updateSingle({Id : '88b57760-a77e-11e5-9d84-794973810f39',
            'OfferCode' : actualdata.result[0].offerCode})
        .then(function(actualdata){
            response.status(200).json(actualdata); 
            console.log('Finally Done!!!')
         });
     }
     }).catch(err => {
      response.send({success: false, msg: 'Error getting the data', err: err });
      
    })
});

//Insert New Deal into  old backend from scratch,first shopid into NearMe.
app.get('/data/AddToNearMe',function(request,response){
   
  var _el = new Everlive({
      appId: APP_ID_OLD,
      scheme: SCHEME
  });
  
  //var query = new Everlive.Query();
       
  _el.data('NearMe').create({'Title' : 'Shack Bar & Grill','Num_Deals': 1 ,'Street':'26-28 Hilton Street','City': 'Manchester','Country':'United Kingdom','Postcode':'M1 2EH','Location':{longitude:-2.232441 , latitude : 53.481904}}).then(function(actualdata){
  console.log('Retrieving the query data ');
  response.status(200).json(actualdata);
  
   
  }).catch(err => {
      response.send({success: false, msg: 'Error creating the data', err: err });
      
    })
});


//Insert New Deal into deals table old backend from scratch.
app.get('/data/InsertDeal1',function(request,response){
  
  var _el = new Everlive({
      appId: APP_ID_OLD,
      scheme: SCHEME
  });
  
  //var query = new Everlive.Query();
       
  _el.data('Deals')
  .create({
   'DealType' : 'NearMe',
  'Availability': 'Everyday 10.00am – 5.00pm',
  'offer':'Cake and Hot Drink for £4.20',
  'Title': 'Cake and Hot Drink for £4.20',
  'Description':'At The Left Bank cafe bar you can enjoy hand-crafted coffees, seasonal British cuisine or fine local ales on Manchester’s sunniest riverside terrace.\
  SPECIAL OFFER: Treat yourself to a piece of cake and regular hot drink for just £4.20.',
'Summary':'Treat yourself to a piece of cake and regular hot drink for just £4.20.',
'Location':{longitude:-2.25339 , latitude : 53.48135},
'Merchant':'People\'s History Museum',
'Website':'http://www.phm.org.uk/',
'Street' : 'Left Bank, Spinningfields',
'City':'Manchester',
'Postcode' :'M3 3ER',
'Country':'United Kingdom',
'Phone' :'0161 838 9190',
'offerCode' : 'BUZ02PHM',
'ShopId':'33cb51c0-a8c3-11e5-8701-2f1aabb5191d',
'Type' : 'Event'})
.then(function(actualdata){
  console.log('Retrieving the query data ');
  response.status(200).json(actualdata);
  console.log('Successfully done!!!')
   
  }).catch(err => {
      response.send({success: false, msg: 'Error creating the data', err: err });
      
    })
});



/*****************/
//Listen to the port 8000
app.listen(PORT, function () {
  console.log('Buzzin Old to New Database migration app listening on port 8000!');
});